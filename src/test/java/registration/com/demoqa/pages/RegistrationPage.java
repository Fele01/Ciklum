package registration.com.demoqa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import registration.com.demoqa.enums.Marital;
import registration.com.demoqa.enums.Salutation;

import java.util.List;

public class RegistrationPage {

    // ELEMENTS
    @FindBy(how = How.ID, using = "name_3_firstname")
    private WebElement firstNameField;
    @FindBy(how = How.ID, using = "name_3_lastname")
    private WebElement lastNameField;
    @FindBy(how = How.XPATH, using = "//input[@type='radio']")
    private List<WebElement> maritalStatusButton;
    @FindBy(how = How.XPATH, using = "//input[@type='checkbox']")
    private List<WebElement> hobbyButton;
    @FindBy(how = How.ID, using = "dropdown_7")
    private WebElement countryField;
    @FindBy(how = How.ID, using = "mm_date_8")
    private WebElement monthField;
    @FindBy(how = How.ID, using = "dd_date_8")
    private WebElement dayNrField;
    @FindBy(how = How.ID, using = "yy_date_8")
    private WebElement yearField;
    @FindBy(how = How.ID, using = "phone_9")
    private WebElement telephoneField;
    @FindBy(how = How.ID, using = "username")
    private WebElement usernameField;
    @FindBy(how = How.ID, using = "email_1")
    private WebElement emailField;
    @FindBy(how = How.ID, using = "password_2")
    private WebElement password1Field;
    @FindBy(how = How.ID, using = "confirm_password_password_2")
    private WebElement password2field;
    @FindBy(how = How.NAME, using = "pie_submit")
    private WebElement continueBtn;


// ERRORS

    @FindBy(how = How.XPATH, using = "//*[@id=\"pie_register\"]/li[1]/div[1]/div[2]/span")
    private WebElement firstNameError;
    @FindBy(how = How.XPATH, using = "//*[@id=\"pie_register\"]/li[1]/div[1]/div[2]/span")
    private WebElement lastNameError;
    @FindBy(how = How.XPATH, using = "//*[@id=\"pie_register\"]/li[3]/div/div[2]/span")
    private WebElement hobbyError;
    @FindBy(how = How.XPATH, using = "//*[@id=\"pie_register\"]/li[6]/div/div/span")
    private WebElement telephoneError;
    @FindBy(how = How.XPATH, using = "//*[@id=\"pie_register\"]/li[7]/div/div/span")
    private WebElement usernameError;
    @FindBy(how = How.XPATH, using = "//*[@id=\"pie_register\"]/li[8]/div/div/span")
    private WebElement emailError;
    @FindBy(how = How.XPATH, using = "//*[@id=\"pie_register\"]/li[11]/div/div/span")
    private WebElement password1Error;
    @FindBy(how = How.XPATH, using = "//*[@id=\"pie_register\"]/li[12]/div/div/span")
    private WebElement password2Error;

    @FindBy(how = How.CLASS_NAME, using = "piereg_message")
    private WebElement succesfullLoginMessage;

    /////////////////
    public void register(String firstName, String lastName, String country, String month,
                         String dayNr, String year, String telephone, String username, String email, String password1, String password2,
                         String firstNameError, String lastNameError, String hobbyError, String telephoneError, String usernameError,
                         String emailError, String password1Error, String password2Error, String successfulLoginMessage) throws InterruptedException {

        Thread.sleep(2000);
        firstNameField.clear();
        firstNameField.sendKeys(firstName);
        lastNameField.clear();
        lastNameField.sendKeys(lastName);
        setMarital(Marital.SINGLE);
        setHobby(Salutation.DANCE);
        setCountry(country);
        setMonth(month);
        setDay(dayNr);
        setYear(year);
        telephoneField.clear();
        telephoneField.sendKeys(telephone);
        usernameField.clear();
        usernameField.sendKeys(username);
        emailField.clear();
        emailField.sendKeys(email);
        password1Field.clear();
        password1Field.sendKeys(password1);
        password2field.clear();
        password2field.sendKeys(password2);

    }

    public void setMarital(Marital marital){
        switch (marital){
            case SINGLE:
                maritalStatusButton.get(0).click();
                break;
            case MARRIED:
                maritalStatusButton.get(1).click();
                break;
            case DIVORCED:
                maritalStatusButton.get(2).click();
                break;
        }
    }

    public void setHobby(Salutation salutation){
        switch (salutation) {
            case DANCE:
                hobbyButton.get(0).click();
                break;
            case READING:
                hobbyButton.get(1).click();
                break;
            case CRICKET:
                hobbyButton.get(2).click();
                break;
        }
    }
    public void setCountry(String cName){
        Select countryList = new Select(countryField);
        countryList.selectByVisibleText(cName);
    }
    public void setMonth(String mName){
        Select monthList = new Select(monthField);
        monthList.selectByVisibleText(mName);
    }
    public void setDay(String dNumber){
        Select dayList = new Select(dayNrField);
        dayList.selectByVisibleText(dNumber);
    }
    public void setYear(String yNumber){
        Select yearList = new Select(yearField);
        yearList.selectByVisibleText(yNumber);
    }


//ERRORS
    public String getFirstNameError(){
        try {
            return firstNameError.getText();
        }catch (NoSuchElementException e) {
            return "";
        }
    }
    public String getLastNameError(){
        try{
            return lastNameError.getText();
        }catch (NoSuchElementException e) {
            return "";
        }
    }
    public String getHobbyError(){
        try {
            return hobbyError.getText();
        }catch (NoSuchElementException e) {
            return "";
        }
    }
    public String getTelephoneError(){
        try {
            return telephoneError.getText();
        }catch (NoSuchElementException e) {
            return "";
        }
    }
    public String getUsernameError(){
        try {
            return usernameError.getText();
        }catch (NoSuchElementException e) {
            return "";
        }
    }
    public String getEmailError(){
        try {
            return emailError.getText();
        }catch (NoSuchElementException e) {
            return "";
        }
    }
    public String getPassword1Error(){
        try{
            return password1Error.getText();
        }catch (NoSuchElementException e) {
            return "";
        }

    }
    public String getPassword2Error(){
        try{
            return password2Error.getText();
        }catch (NoSuchElementException e) {
            return "";
        }

    }

    public String getSuccesfullLoginMessage(){
        try {
            return succesfullLoginMessage.getText();
        }catch (NoSuchElementException e){
            return "";
        }
    }
}


