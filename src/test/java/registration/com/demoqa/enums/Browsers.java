package registration.com.demoqa.enums;

public enum Browsers {
    CHROME,
    FIREFOX,
    IE
}
