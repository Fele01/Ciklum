package registration.com.demoqa.enums;

public enum Salutation {
    DANCE,
    READING,
    CRICKET
}
