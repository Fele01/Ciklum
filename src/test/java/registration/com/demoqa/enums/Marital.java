package registration.com.demoqa.enums;

public enum Marital {
    SINGLE ,
    MARRIED,
    DIVORCED
}
