package registration.com.demoqa.tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import registration.com.demoqa.base.BaseTest;
import registration.com.demoqa.pages.RegistrationPage;

import java.util.*;

public class RegistrationTest extends BaseTest {

    @DataProvider(name = "RegistrationDataProvider")
    public Iterator<Object[]> registrationDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        List<String> firstNameList = Arrays.asList("Jan van", "Chack", "Klark n", "John", "Bat", "Tim", "Dave o", "Pay", "Lazy", "Jack &");
        List<String> lastNameList = Arrays.asList("Dam", "Norris", "Kent", "Daw", "Man", "Los", "Core", "Pal", "Cat", "Johnes");
        List<String> countryList = Arrays.asList("Poland", "Azerbaijan", "Afghanistan", "Austria", "Brazil", "China", "Greece", "Libya", "Mexico", "Mexico");
        List<String> telephoneNumberList = Arrays.asList("0040758890332", "0040755560332", "0040759980332", "0040753870332", "0040753876532", "0040753843232", "0040753821132", "0040753811132", "0040753870331", "0040753870333");
        List<String> usernameList = Arrays.asList("PolandDam", "AzerbaijanNorris", "KentAfghanistan", "ManAustria", "BatBrazil", "ChinaBat", "GreeceBat", "LibyaPal", "MexicoCat", "MexicoPal");
        List<String> passwordList = Arrays.asList("sqwqeweqe12", "qwert123141", "Kentea121456", "125121qwreasd", "asdasdqweqwe112233441", "asdqweQWEASD123", "1234QWEQWEAA", "QWETYU12412", "PORT12311", "Johnes11123");
        for (int j = 0; j <= 4; j++) {
            Random rand = new Random();
            int i = rand.nextInt(firstNameList.size()) + 2;
            int dayOfBirth = rand.nextInt(31) + 1;
            int monthOfBirth = rand.nextInt(12) + 1;
            int yearOfBirth = rand.nextInt(2014) + 1950;
            String emailAdress = firstNameList.get(i) + lastNameList.get(i) + "@email.com";
            dp.add(new String[] {
                firstNameList.get(i), lastNameList.get(i), countryList.get(i), Integer.toString(monthOfBirth), Integer.toString(dayOfBirth), Integer.toString(yearOfBirth), telephoneNumberList.get(i),
                        usernameList.get(i), emailAdress, passwordList.get(i), passwordList.get(i), "", "", "", "", "", "", "", ""});
        firstNameList.remove(firstNameList.get(i));
        lastNameList.remove(lastNameList.get(i));
        countryList.remove(countryList.get(i));
        telephoneNumberList.remove(telephoneNumberList.get(i));
        usernameList.remove(usernameList.get(i));
        passwordList.remove(passwordList.get(i));
        }
        for (int k = 0; k<=firstNameList.size();k++){
            System.out.println("The remaining users are:");
            System.out.println(firstNameList.get(k));
            System.out.println(lastNameList.get(k));
        }
        return dp.iterator();
    }



    @Test(dataProvider = "RegistrationDataProvider")
    public void registrationTest(String firstName, String lastName, String country, String month,
                                 String dayNr, String year, String telephone, String username, String email, String password1, String password2,
                                 String firstNameError, String lastNameError, String hobbyError, String telephoneError, String usernameError,
                                 String emailError, String password1Error, String password2Error, String successfulLoginMessage) throws InterruptedException {

        RegistrationPage registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
        registrationPage.register(firstName, lastName, country, month, dayNr, year, telephone, username, email, password1, password2,
                                  firstNameError, lastNameError, hobbyError, telephoneError, usernameError, emailError, password1Error, password2Error, successfulLoginMessage);
        Assert.assertEquals(registrationPage.getFirstNameError(), firstNameError, "Checking first name error");
        Assert.assertEquals(registrationPage.getLastNameError(),lastNameError, "Checking last name error");
        Assert.assertEquals(registrationPage.getHobbyError(), hobbyError, "Checking hobby error");
        Assert.assertEquals(registrationPage.getTelephoneError(),telephoneError, "Checking telephone error");
        Assert.assertEquals(registrationPage.getUsernameError(), usernameError, "Checking username error");
        Assert.assertEquals(registrationPage.getEmailError(), emailError, "Checking email error");
        Assert.assertEquals(registrationPage.getPassword1Error(), password1Error, "Checking password error");
        Assert.assertEquals(registrationPage.getPassword2Error(), password2Error, "Checking password confirmation error");
//        Assert.assertEquals(registrationPage.getSuccesfullLoginMessage(), successfulLoginMessage , "Check successful registration");
    }
}
