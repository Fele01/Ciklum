package registration.com.demoqa.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import registration.com.demoqa.base.BaseTest;
import registration.com.demoqa.verify.Compare;

public class test1ValidateURL extends BaseTest{
    @Test
    public void testcaseURLvalidation() throws InterruptedException {
        Thread.sleep(2000);
        Assert.assertTrue(Compare.validateURL(driver, "http://demoqa.com/registration/"), "[Error  Actual URL does not match with Expected URL]");
    }
}
