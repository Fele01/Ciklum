package registration.com.demoqa.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import registration.com.demoqa.enums.Browsers;
import registration.com.demoqa.utils.WebBrowsers;
import registration.com.demoqa.verify.Compare;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    public WebDriver driver;

    @BeforeTest
    public void setUp() {
        driver = WebBrowsers.getDriver(Browsers.CHROME);
        driver.manage().window().maximize();
        driver.get("http://demoqa.com/registration/");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

//    @AfterTest(alwaysRun = true)
//    public void tearDown() {
//        if (driver != null){
//            driver.quit();
//        }
//    }
}
