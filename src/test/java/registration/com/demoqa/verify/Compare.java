package registration.com.demoqa.verify;

import org.openqa.selenium.WebDriver;

public class Compare {
    public static boolean validateURL (WebDriver driver, String expURL){
        boolean result =  false;
        String currentURL= driver.getCurrentUrl();
        System.out.println(currentURL);
        System.out.println(expURL);
        if(driver.getCurrentUrl().equalsIgnoreCase(expURL))
        {
            result = true;
        }
        return result;
    }
}
